# image-liker

A ridiculously and dangerously simple Web app used to rate pictures with a simple yes/no. In my use case, I have many hundreds of generated images to sift through, noting the ones I want to use and discarding the rest.

Data is stored in username:filename pairs, allowing several people to rate images. A report on the number of likes per filename can be found at `http://localhost:5000/report`.

## Building and Running the Project

```
docker-compose up --build
```

## Where to Put your Pictures

Place the images in `static/images`.

## WARNINGS

This was a quick hack on a Sunday morning. As such, many security best-practices were overlooked. There is no input validation anywhere; different files can have the same name, leading to undefined results; there is nothing like error checking of any kind anywhere; anyone can enter an arbitrary username and see what was liked by that username. If you'd like to share this with more than a person or two on your local network, then BEWARE.

## License
Image-Liker is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Image-Liker is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Image-Liker. If not, see <https://www.gnu.org/licenses/>. 

## Project status
!["Under Construction"](https://www.muppetlabs.com/~deadman/obama2008/under-construction.png)
