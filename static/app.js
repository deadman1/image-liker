let username;

window.onload = () => {
    try {
        const cookieString = document.cookie;
        const cookiesList = cookieString.split("; ");
        const usernameCookie = cookiesList.find(cookie => cookie.startsWith("username="));
        username = usernameCookie ? usernameCookie.split("=")[1] : undefined;
    } catch (error) {
        console.error(error);
    }

    const usernameForm = document.getElementById('username-form');
    const usernameInput = document.getElementById('username-input');
    const greeting = document.getElementById('greeting');

    if (username) {
        greeting.innerText = `Hello, ${username}!`;
        greeting.style.display = 'block';
    } else {
        usernameForm.style.display = 'block';
    }

    document.getElementById('submit-button').addEventListener('click', () => {
        username = usernameInput.value;
        document.cookie = `username=${username}`;
        usernameForm.style.display = 'none';
        greeting.innerText = `Hello, ${username}!`;
        greeting.style.display = 'block';
	location.reload();
    });

    fetch('/api/images')
        .then(response => response.json())
        .then(images => {
            const container = document.getElementById('image-container');
            images.forEach(image => {
                // Construct the div with the image and the button
                const div = document.createElement('div');
                const img = document.createElement('img');
                img.src = image;
		img.addEventListener('click', () => {
                    window.open(image, '_blank');
		});
		div.appendChild(img);

                const button = document.createElement('button');
		button.innerText = 'Not Liked';
		button.className = 'button';
		button.addEventListener('click', toggleLike);
		div.appendChild(button);		
                
                // Fetch like state for the image
                const filename = img.src.split('/').pop();
                fetch(`/api/likes/${filename}`)
                    .then(response => response.json())
                    .then(data => {
			if (data.liked) {
                            button.innerText = 'Liked';
                            button.classList.add('liked');
			}
                    });

                button.addEventListener('click', toggleLike);
                div.appendChild(button);

                container.appendChild(div);
            });
        });
};

function toggleLike() {
    const button = this;
    const img = this.previousSibling;

    // Extract filename from img.src
    const filename = img.src.split('/').pop();

    if (!username) {
        alert('Please enter your username before liking an image.');
        return;
    }

    const like = button.innerText === 'Liked';
    fetch(`/api/likes/${filename}`, {
        method: like ? 'DELETE' : 'POST',
    })
        .then(response => response.json())
        .then(data => {
            button.innerText = data.liked ? 'Liked' : 'Not Liked';
            if (data.liked) {
                button.classList.add('liked');
            } else {
                button.classList.remove('liked');
            }
        });
}
