from database import db

class Like(db.Model):
    #id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), nullable=False, primary_key=True)
    filename = db.Column(db.String(128), nullable=False, primary_key=True)
