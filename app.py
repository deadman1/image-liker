from flask import Flask, request, make_response, jsonify, render_template, url_for
from likes import Like
from database import db

import os
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)

with app.app_context():
    db.create_all()

@app.route('/')
def home():
    username = request.cookies.get('username')
    return render_template('index.html', username=username)

@app.route('/api/images')
def get_images():
    images = os.listdir('static/images/')
    return jsonify([url_for('static', filename=f'images/{image}') for image in images])

@app.route('/api/likes', methods=['POST', 'DELETE'])
def update_likes():
    username = request.cookies.get('username')
    filename = request.json.get('filename')
    if request.method == 'POST':
        like = Like(username=username, filename=filename)
        db.session.add(like)
    elif request.method == 'DELETE':
        Like.query.filter_by(username=username, filename=filename).delete()
    db.session.commit()
    return '', 204

@app.route('/api/likes/<filename>', methods=['GET', 'POST', 'DELETE'])
def handle_likes(filename):
    if request.method == 'GET':
        like = Like.query.filter_by(username=request.cookies.get('username'), filename=filename).first()
        return jsonify(liked=bool(like))
    elif request.method == 'POST':
        like = Like(username=request.cookies.get('username'), filename=filename)
        db.session.add(like)
        db.session.commit()
        return jsonify(liked=True)
    elif request.method == 'DELETE':
        Like.query.filter_by(username=request.cookies.get('username'), filename=filename).delete()
        db.session.commit()
        return jsonify(liked=False)

@app.route('/report')
def report():
    report_data = db.session.query(Like.filename, db.func.count(Like.username)).group_by(Like.filename).all()
    report = {filename: count for filename, count in report_data}
    return jsonify(report)

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(host='0.0.0.0')
