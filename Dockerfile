FROM python:3.9-alpine

WORKDIR /app

COPY . .

RUN pip install flask flask_sqlalchemy gunicorn

CMD ["gunicorn", "-b", ":5000", "app:app"]
